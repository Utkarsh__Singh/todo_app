// // selectors

const addButton = document.querySelector(".addButton");
const inputValue = document.querySelector('.input');
const container = document.querySelector('.container');
const saved = document.querySelector(".saved")
// const uuid = uuidv4();
// console.log(uuid);
// // const input_div=document.querySelector('.input_div')

// // event listeners

addButton.addEventListener("click", () => {
    const uuid=uuidv4()
    if (inputValue.value !== "") {
        localStorage.setItem(uuid, JSON.stringify(inputValue.value));

    }
    else {
        alert("Please enter a todo")
    }
    // Object.values(localStorage).forEach((task) => {
            // console.log(task)
    const task = localStorage.getItem(uuid)
    
    console.log(task)
    createDiv(task.replaceAll('"', ''),uuid)
    // })
})

// // input_div.addEventListener("submit", () => {
// //     createDiv(inputValue.value);
// // })
// // functions
function createDiv(todoItem,uuid) {
    let input = document.createElement("input");
    input.value = todoItem;
    input.disabled = true;
    input.classList.add("item_input");
    input.type = "text";
    
    // creating a div with elements
    let itemBox = document.createElement("div");
    itemBox.classList.add("item");

    // creating edit button 
    let editButton = document.createElement("button");
    editButton.innerHTML="Edit";
    editButton.classList.add("editButton");

    // creating remove button
    let removeButton = document.createElement("button");
    removeButton.innerHTML="Remove";
    removeButton.classList.add("removeButton");

    // appending all child element in container
    container.appendChild(itemBox);

    itemBox.appendChild(input);
    itemBox.appendChild(editButton);
    itemBox.appendChild(removeButton);

    editButton.addEventListener("click", () => {
        edit(input,uuid);
        
    })
    
    removeButton.addEventListener("click", () => {
        remove(itemBox,uuid);
    })
    inputValue.value=""
    
}

function edit(input,uuid) {
    // console.log(input)
    input.disabled = !input.disabled;
    localStorage.setItem(uuid, input.value)
}

function remove(itemBox,uuid) {
    container.removeChild(itemBox);
    localStorage.removeItem(uuid);
}



// inserting data in local storage
// localStorage.setItem("name", "Utkarsh");
// using key to fetch data from localStorage
// localStorage.getItem("name")
// clearing localstorage
// localStorage.clear();



// const todos = [
//   {
//     item: "To complete JavaScript",
//     isCompleted: false
//   },
//   {
//     item: "Meditation",
//     isCompleted: true
//   }
// ]

// localStorage.setItem("todos", JSON.stringify(todos));

// localStorage.getItem("todos")
